H.M. Hashemian, “Application of Advanced Technology to Improve Plant Performance in Nuclear Power Plants”, AMS Corporation, AMS Technology Center, 9119 Cross Park Drive, Knoxville, Tennessee 37923, USA,IAEA-CN-164-13KS

Jones, E., Oliphant, T., Peterson, P., & others. (2020). SciPy: Open source scientific tools for Python. Retrieved from "http://www.scipy.org/"



To demonstrate the functionality of the software we
have written, we generated an arbitrary signal so that we
can visualize our analysis. It is important to note that all
inputs, such as the signal itself, the sampling rate, time,
and most other variables in the code, can be effortlessly
changed to handle different specs and environments.
The generated signal is shown below. The signal is a
summation of signals with various frequencies. Our pro-
gram takes our generated signal and extrapolates desired
information, which for the purpose of this example will be
physical information of signals that have a frequency of
less than 20 Hz.

We used a sampling rate of 300
and sampled for 30 seconds to
generate this signal. The signal
is comprised of 3 signals with
frequencies of 20, 40, and 80
Hz and a randomly generated
Gaussian distribution to emu-
late noise.


To filter out noise, we will use SciPy’s Butterworth 1 low-pass
filter (REFERENCE 2). A low-pass filter is a filter that seeks to
remove or remove all frequencies above a set cutoff. So, if you do
not want your filtered signal to have frequencies above 100 Hz,
set the cutoff to 100 Hz.

Since we are trying to capture data
from signals with 20 Hz or less, we
will set our cutoff frequency to 30 Hz.

The graph shows 0.25 seconds of the chart above. There are 75 filtered samples plotted, and approximately 5 complete cycles, meaning that there are approximately 20 cycles a
second. This shows that we’ve extracted a 20 Hz signal from our generated signal.

We chose to use an
simple moving average
(SMA) over a exponen-
tial moving average be-
cause EMA’s are more
subject to recency bias.
We do not want spikes
or fluctuations in our
smoothed signal.

Being able to identify changes in our signal is essential. When monitoring systems, it’s useful to know when a unit of measure is rising and
falling so that we know if systems are performing as expected. To include this functionality, we came up with two approaches. Both methods deal with computing the derivative at a given sample point. The methods differ in how often they compute the derivative.

While more accurate, taking the
derivative at every sample does
not capture the trend of the signal
as well as every taking the derivative every 30 samples because our smoothed signal is is not entirely smooth.

By checking each sample against the set thresholds, we
can identify when the unit of measurement has breached its
set bounds. Following, it is not a big step to notify whoever
or whatever is monitoring the system so the appropriate
course of action can be taken.

When saving our data for later retrieval, we
should only save as much as we need. So,
every N samples, we write to a CSV file the unit, timestamp, the sample number, and the
data itself. By using incremental saving, we can capture the trends and structure of our
data at a fraction of the space required to
store it all.

N = 100 is almost
identical to our
smoothed signal. As
N gets higher, we
lose accuracy, but
even N = 800 isn’t
drastically different.



